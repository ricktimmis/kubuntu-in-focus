# Kubuntu in Focus

A community magazine developed using Open Source tools. 

* Take a look at our [repo](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/tree/master)
* Take a look at the [Wiki Documentation](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/wikis/home)
* Pick or create an Issue from our [Issues Board](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/issues)

## Getting Started

To get Read/Write access to KDE Invent you'll need a [KDE Identity](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/issues) it only takes a couple of minutes and once you have your KDE Identity you can use it many other places across kde.org too.

## Overview

This projects intention is to make it 'Super Easy' for all members of the community to contribute, but also to provide an easy onboarding experience for learning about the development tools used by KDE.

Our Magazine consists of articles, and we track their requirements via [Issues](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/issues).Using KDE Invent makes it a really simple to pick up an issue and start writing an article, which will become your contribution to the Magazine.

Here are the steps :-

* GoTo the [Issues Board](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/issues)
* Click on an Issue
* Click the 'Create merge request' button
* Click the 'Open in Web IDE' button

So, here's what just happened.

1. The Issues board holds all the ideas for articles alond with their status 'Open' 'To Do' 'Doing' and 'Closed'
2. Clicking an Issue on the board takes you to its details, where you can see what the requirements are
3. Creating a Merge request makes a safe copy (git branch) of everything ( all the files) in the project, and assigns the work to your
4. The Web IDE makes it easy to get to work 

## Writing an Article

We are using MarkDown to create our articles. [The Mark Down Guide](https://www.markdownguide.org/) is your friend, and writing Markdown is easy ( Edit this file to see for your self)

In the [Web IDE](https://www.youtube.com/watch?v=Y2SsnCHJd2w) Open the Issue_<Number> folder (i.e the current issue [Milestone](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/milestones) being worked on) and use the + icon to create a New File, or Click on an existing File to begin editing it. (Remember, this is totally safe because your Create Merge Request made a copy of everything - So please Go Ahead and play around)

Go head, and write your article. Once you're done; Use the 'Commit' button to save your work. Commit is again another [Git](https://www.freecodecamp.org/news/an-introduction-to-git-for-absolute-beginners-86fa1d32ff71/) term, but it is essentially saving your work. Notice that it also enables you to add a Commit Message. This is really useful for helping a reviewer to understand what your changes are about.

## What happens next

Once you have 'committed' your work it is saved to the 'temporary_branch_name' which was created when your 'Create merge request'. Other authors can review your article and help contribute to it. Project maintainers, can merge the work into the Main the branch, where it can be picked up by the type setters who use [Scribus 1.5.5](https://www.scribus.net/) to turn it into the finished article for the magazine.

## Becoming a Scribus Type setters

Well, we've made this easy too. However, there are a coupled of extra steps

1. Click on the [Issue Board](https://invent.kde.org/ricktimmis/kubuntu-in-focus/-/boards/3600)
2. Pick up an entry from the Type Setting cloumn
3. 'Clone the branch' using Git to get a locally stored copy on your machine. [How to Clone from GitLab](https://www.youtube.com/watch?v=l_Fj83MDJ8A)
4. Navigate to the Issue_<Number> directory, and open the K_in_F-Issue-x.sla with Scribus ( where x = the issue number e.g 5 )
5. Type set the article and add screenshots / images etc.. and Save in Scribus
6. Use git add . to 'Stage' your changes
7. 'Commit' your work
8. Use 'git push' to upload your work back to invent.kde.org

## Branding and Artwork

### KDE Artwork
https://kde.org/stuff/clipart.php
